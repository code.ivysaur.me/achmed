# achmed

![](https://img.shields.io/badge/written%20in-Golang-blue)

A TCP reverse proxy with SNI-based routing and Let's Encrypt support.

`achmed` (ACME H. Daemon) allows you to terminate TLS with Let's Encrypt support for any TCP-based service.

## Usage


```
Usage:
  achmed [OPTIONS]...

Options:
  --bind [HOST]:PORT   Change bind address (default: ":443")
                        Specify multiple times to add additional bind addresses.
  --cache-dir PATH     Directory for certificate cache (default: .)
                        The directory will be created if it doesn't exist.
  DNSNAME=UPSTREAM     Listen on DNSNAME, forward connections to tcp:UPSTREAM
  --help               Display this message

```


## Changelog

2017-06-03 1.0
- Initial public release
- [⬇️ achmed-1.0-win64.7z](dist-archive/achmed-1.0-win64.7z) *(1,009.91 KiB)*
- [⬇️ achmed-1.0-src.zip](dist-archive/achmed-1.0-src.zip) *(2.94 KiB)*
- [⬇️ achmed-1.0-linux64.tar.gz](dist-archive/achmed-1.0-linux64.tar.gz) *(1.40 MiB)*

